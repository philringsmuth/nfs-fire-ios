//
//  DetailViewController.m
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 1/21/15.
//  Copyright (c) 2015 Nebraska Forest Services. All rights reserved.
//

#import "DetailViewController.h"
#import "NFSDataSource.h"
#import "NFSConstants.h"
#import "NFSLocation.h"
#import "NFSFoam.h"
#import "NFSSeat.h"
#import "NFSColorUtility.h"

#import <CoreLocation/CoreLocation.h>

@implementation DetailViewController
{
	CLLocationCoordinate2D _currentLocation;
	
	BOOL _showPlanes;
	BOOL _showFoams;
	BOOL _showSeats;
    BOOL _showList;
    BOOL _showMap;
	
	NSInteger _selectedRow;
	
	UIColor *_viewColor;
	CLLocation *_initialLocation;
}


#pragma mark Apple Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	[self setCurrentLocation];
	[self listButtonPressed];
	[self planesButtonPressed];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark IBAction Methods

- (IBAction) fingerDownInView:(id)sender
{
	_viewColor = [sender backgroundColor];
	
	[sender setBackgroundColor:[UIColor whiteColor]];
}

- (IBAction) fingerExitedView:(id)sender
{
	[sender setBackgroundColor:_viewColor];
}

- (IBAction) fingerUpInView:(id)sender
{
	if (sender == _backButton)
	{
		[self backButtonPressed];
	}
	
	if (sender == _listButton)
	{
		[self listButtonPressed];
	}
	
	if (sender == _mapButton)
	{
		[self mapButtonPressed];
	}
	
	if (sender == _planesButton)
	{
		[self planesButtonPressed];
	}
	
	if (sender == _seatsButton)
	{
		[self seatsButtonPressed];
	}
	
	if (sender == _foamButton)
	{
		[self foamsButtonPressed];
	}
}


#pragma mark Tableview Datasource Methods

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (_showPlanes)
	{
		return _planes.count;
	}
	
	else if (_showFoams)
	{
		return _foams.count;
	}
	
	else if (_showSeats)
	{
		return _seats.count;
	}
	
	return 0;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
	
	NSString *cellTitle;
	NSString *cellSubtitle;
	
	if (_showPlanes)
	{
		NFSLocation *aerialApplicator = (NFSLocation *) [_planes objectAtIndex:indexPath.row];
		int distanceInMiles = [self distanceBetweenLocations:_currentLocation secondLocation:aerialApplicator.coordinate];
		float degrees = [self getHeadingForDirectionFromCoordinate:_currentLocation toCoordinate:aerialApplicator.coordinate];
		NSString *direction = [self bearingToCompassDirection:degrees];
		
		cellTitle = aerialApplicator.businessName;
		cellSubtitle = [NSString stringWithFormat:@"%d miles %@", distanceInMiles, direction];
	}
	
	else if (_showFoams)
	{
        NFSFoam *foamRetardant = (NFSFoam *) [_foams objectAtIndex:indexPath.row];
        int distanceInMiles = [self distanceBetweenLocations:_currentLocation secondLocation:foamRetardant.coordinate];
        float degrees = [self getHeadingForDirectionFromCoordinate:_currentLocation toCoordinate:foamRetardant.coordinate];
        NSString *direction = [self bearingToCompassDirection:degrees];
        
        cellTitle = foamRetardant.location;
        cellSubtitle = [NSString stringWithFormat:@"%d miles %@", distanceInMiles, direction];

	}
	
	else if (_showSeats)
	{
        NFSSeat *seat = (NFSSeat *) [_seats objectAtIndex:indexPath.row];
        int distanceInMiles = [self distanceBetweenLocations:_currentLocation secondLocation:seat.coordinate];
        float degrees = [self getHeadingForDirectionFromCoordinate:_currentLocation toCoordinate:seat.coordinate];
        NSString *direction = [self bearingToCompassDirection:degrees];
        
        cellTitle = seat.county;
        cellSubtitle = [NSString stringWithFormat:@"%d miles %@", distanceInMiles, direction];

	}
	
	[cell.textLabel setText:cellTitle];
	[cell.detailTextLabel setText:cellSubtitle];
	
	return cell;
}

#pragma mark Tableview Delegate Methods

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	_selectedRow = indexPath.row;
	
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	
	if (_showPlanes)
	{
		NFSLocation *aerialApplicator = (NFSLocation *) [_planes objectAtIndex:indexPath.row];
		
        if([[aerialApplicator.status uppercaseString] isEqual:@"ACTIVE"])
        {
            [self showPlane:aerialApplicator];
        }
	}
	
	else if (_showFoams)
	{
        NFSFoam *foamRetardant = (NFSFoam *) [_foams objectAtIndex:indexPath.row];
        
        [self showFoam:foamRetardant];
	}
	
	else if (_showSeats)
	{
        NFSSeat *seat = (NFSSeat *) [_seats objectAtIndex:indexPath.row];
        
        [self showSeat:seat];
	}
}


#pragma mark MKMapViewDelegate

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
	NSString *identifier;
    
    //Used for planes
	if ([annotation isKindOfClass:[NFSLocation class]])
	{
        identifier = @"NFSLocation";
		MKAnnotationView *annotationView = (MKAnnotationView *) [_mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
		
		if (annotationView == nil)
		{
			annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
			
			[annotationView setEnabled:YES];
			[annotationView setCanShowCallout:NO];
			
			NFSLocation *location = (NFSLocation *) annotation;
			
			if ([location.numberOfAircraft isEqualToString:@"1"])
			{
				[annotationView setImage:[UIImage imageNamed:@"pin01"]];
			}
			
			if ([location.numberOfAircraft isEqualToString:@"2"])
			{
				[annotationView setImage:[UIImage imageNamed:@"pin02"]];
			}
			
			if ([location.numberOfAircraft isEqualToString:@"3"])
			{
				[annotationView setImage:[UIImage imageNamed:@"pin03"]];
			}
			
			if ([location.numberOfAircraft isEqualToString:@"4"])
			{
				[annotationView setImage:[UIImage imageNamed:@"pin04"]];
			}
			
			if ([location.numberOfAircraft isEqualToString:@"5"])
			{
				[annotationView setImage:[UIImage imageNamed:@"pin05"]];
			}
			
			
			
			annotationView.centerOffset = CGPointMake(0, -10);
		}
		else
		{
			[annotationView setAnnotation:annotation];
		}
		
		return annotationView;
	}
    
    //Used for foams
    else if ([annotation isKindOfClass:[NFSFoam class]])
    {
        identifier = @"NFSFoam";
        MKAnnotationView *annotationView = (MKAnnotationView *) [_mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (annotationView == nil)
        {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            
            [annotationView setEnabled:YES];
            [annotationView setCanShowCallout:NO];
            
            [annotationView setImage:[UIImage imageNamed:@"pin"]];
     
            annotationView.centerOffset = CGPointMake(0, -10);
        }
        else
        {
            [annotationView setAnnotation:annotation];
        }
        
        return annotationView;
    }
    
    //Used for seats
    else if([annotation isKindOfClass:[NFSSeat class]])
    {
        identifier = @"NFSSeat";
        MKAnnotationView *annotationView = (MKAnnotationView *) [_mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        
        if (annotationView == nil)
        {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            
            [annotationView setEnabled:YES];
            [annotationView setCanShowCallout:NO];
            
            [annotationView setImage:[UIImage imageNamed:@"pin"]];
            
            annotationView.centerOffset = CGPointMake(0, -10);
        }
        else
        {
            [annotationView setAnnotation:annotation];
        }
        
        return annotationView;
    }
	
	return nil;
}

- (void) mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
	[mapView deselectAnnotation:view.annotation animated:YES];
	if([view.annotation isKindOfClass:[NFSLocation class]])
    {
        [self showPlane:(NFSLocation*) view.annotation];
    }
    else if([view.annotation isKindOfClass:[NFSFoam class]])
    {
        [self showFoam:(NFSFoam *) view.annotation];
    }
    else if([view.annotation isKindOfClass:[NFSSeat class]])
    {
        [self showSeat:(NFSSeat *) view.annotation];
    }
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
	if (!_initialLocation)
	{
		_initialLocation = userLocation.location;
		
		MKCoordinateRegion region;
		region.center = mapView.userLocation.coordinate;
		region.span = MKCoordinateSpanMake(kLatitudeDelta, kLongitudeDelta);
		
		region = [mapView regionThatFits:region];
		[mapView setRegion:region animated:YES];
		
	}
}


#pragma mark UIAlertView Delegate

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	/*
	 1 = day phone
	 2 = alternate/night phone
	 0 = cancel
	 */
	
	if (buttonIndex == 0)
	{
		return;
	}
	
	NSString *numberToCall;
	
	if (_showPlanes)
	{
		NFSLocation *aerialApplicator = [_planes objectAtIndex:_selectedRow];
		
		if (buttonIndex == 1)
		{
			numberToCall = aerialApplicator.dayPhone;
		}
		
		if (buttonIndex == 2)
		{
			numberToCall = aerialApplicator.alternatePhone;
		}
	}
	
	else if (_showFoams)
	{
        NFSFoam *foamRetardant = [_foams objectAtIndex:_selectedRow];
        
        if (buttonIndex == 1)
        {
            numberToCall = foamRetardant.dayPhone;
        }
        
        if(buttonIndex == 2)
        {
            numberToCall = foamRetardant.nightPhone;
        }
	}
    
    //Not implemented for seats
	
	// http://stackoverflow.com/a/15127098/529554
	NSString *cleanedString = [[numberToCall componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", cleanedString]]];
}


#pragma mark Private Methods

- (void) backButtonPressed
{
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void) listButtonPressed
{
    _showList = YES;
    _showMap = NO;
	[_listButton setBackgroundColor:[NFSColorUtility colorFromHexString:kColorYellow]];
	[_mapButton setBackgroundColor:[UIColor whiteColor]];
	
	[_tableView setUserInteractionEnabled:NO];
	[_tableView setUserInteractionEnabled:NO];
	
	[UIView animateWithDuration:kFadeDuration animations:^{
		[_tableView setAlpha:1.0];
		[_mapView setAlpha:0.0];
	}];
	
	[_tableView setUserInteractionEnabled:YES];
}

- (void) mapButtonPressed
{
    _showMap = YES;
    _showList = NO;
	[_listButton setBackgroundColor:[UIColor whiteColor]];
	[_mapButton setBackgroundColor:[NFSColorUtility colorFromHexString:kColorYellow]];
	
	[_tableView setUserInteractionEnabled:NO];
	[_mapView setUserInteractionEnabled:NO];
	
	[UIView animateWithDuration:kFadeDuration animations:^{
		[_tableView setAlpha:0.0];
		[_mapView setAlpha:1.0];
	}];
	
	[_mapView setUserInteractionEnabled:YES];
}

- (void) planesButtonPressed
{
	[self selectDataButton:_planesButton];
	[self sortPlanesByDistance];
	[self plotPlanes];
	
	_showPlanes = YES;
    _showSeats = NO;
    _showFoams = NO;
	
	[_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
	[_tableView setContentOffset:CGPointZero animated:NO];
}

- (void) seatsButtonPressed
{
	[self selectDataButton:_seatsButton];
	[self sortSeatsByDistance];
	[self plotSeats];
	
	_showPlanes = NO;
	_showSeats = YES;
	_showFoams = NO;
	
	[_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
	[_tableView setContentOffset:CGPointZero animated:NO];
}

- (void) foamsButtonPressed
{
	[self selectDataButton:_foamButton];
    [self sortFoamByDistance];
    [self plotFoam];
    
    _showPlanes = NO;
    _showSeats = NO;
    _showFoams = YES;
	
	[_tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
	[_tableView setContentOffset:CGPointZero animated:NO];
}

- (void) selectDataButton:(UIView *)view
{
	[_planesButton		setBackgroundColor:[UIColor whiteColor]];
	[_seatsButton		setBackgroundColor:[UIColor whiteColor]];
	[_foamButton		setBackgroundColor:[UIColor whiteColor]];
	
	[view setBackgroundColor:[NFSColorUtility colorFromHexString:kColorYellow]];
}

- (NSString *) prepareMessageForAerialApplicator:(NFSLocation *)aerialApplicator
{
	NSString *message = [[NSString alloc] init];
	
	message = [self addText:aerialApplicator.ownerOperator toString:message];
	message = [self addText:[NSString stringWithFormat:@"Day Phone: %@", aerialApplicator.dayPhone] toString:message];
	
	if (aerialApplicator.alternatePhone.length > 0)
	{
		message = [self addText:[NSString stringWithFormat:@"Alternate Phone: %@", aerialApplicator.alternatePhone] toString:message];
	}
	
	message = [self addText:[NSString stringWithFormat:@"Email: %@", aerialApplicator.email] toString:message];
	message = [self addText:[NSString stringWithFormat:@"Zip Code: %@", aerialApplicator.zipCode] toString:message];
	
	message = [self addText:@"" toString:message];
	
	message = [self addText:[NSString stringWithFormat:@"Number of Aircraft: %@", aerialApplicator.numberOfAircraft] toString:message];
	message = [self addText:[NSString stringWithFormat:@"Class A Foam: %@", aerialApplicator.classAFoam] toString:message];
	message = [self addText:[NSString stringWithFormat:@"Radio Frequency: %@", aerialApplicator.radioFrequency] toString:message];
	
	message = [self addText:@"" toString:message];
	
	NSString *tailText = [self tailTextString:aerialApplicator.tailNumber1 capacity:aerialApplicator.gallonCapacity1];
	message = [self addText:tailText toString:message];
	
	if (aerialApplicator.tailNumber2.length > 0)
	{
		NSString *tailText = [self tailTextString:aerialApplicator.tailNumber2 capacity:aerialApplicator.gallonCapacity2];
		message = [self addText:tailText toString:message];
	}
	
	if (aerialApplicator.tailNumber3.length > 0)
	{
		NSString *tailText = [self tailTextString:aerialApplicator.tailNumber3 capacity:aerialApplicator.gallonCapacity3];
		message = [self addText:tailText toString:message];
	}
	
	if (aerialApplicator.tailNumber4.length > 0)
	{
		NSString *tailText = [self tailTextString:aerialApplicator.tailNumber4 capacity:aerialApplicator.gallonCapacity4];
		message = [self addText:tailText toString:message];
	}
	
	if (aerialApplicator.tailNumber5.length > 0)
	{
		NSString *tailText = [self tailTextString:aerialApplicator.tailNumber5 capacity:aerialApplicator.gallonCapacity5];
		message = [self addText:tailText toString:message];
	}
	
	return message;
}

- (NSString *) tailTextString:(NSString *)tailNumber capacity:(NSString *)capacity
{
	return [NSString stringWithFormat:@"Tail Number: %@, %@", tailNumber, capacity];
}

- (NSString *) prepareMessageForFoamRetardant:(NFSFoam *)foamRetardant
{
    NSString *message = [[NSString alloc] init];
    
    message = [self addText:foamRetardant.location toString:message];
    message = [self addText:[NSString stringWithFormat:@"Contact: %@", foamRetardant.contact] toString:message];
    message = [self addText:[NSString stringWithFormat:@"Day Phone: %@", foamRetardant.dayPhone] toString:message];
    
    if (foamRetardant.nightPhone.length > 0)
    {
        message = [self addText:[NSString stringWithFormat:@"Night Phone: %@", foamRetardant.nightPhone] toString:message];
    }
    
    message = [self addText:[NSString stringWithFormat:@"Zip Code: %@", foamRetardant.zipCode] toString:message];

    return message;
}

- (NSString *) prepareMessageForSeat:(NFSSeat *)seat
{
    NSString *message = [[NSString alloc] init];
    
    message = [self addText:seat.county toString:message];
    message = [self addText:[NSString stringWithFormat:@"Status: %@", seat.status] toString:message];
    
    return message;
}


- (NSString *) addText:(NSString *)text toString:(NSString *)string
{
	return [string stringByAppendingString:[NSString stringWithFormat:@"%@\n", text]];
}

- (void) setCurrentLocation
{
    if(self.locationManager == nil)
    {
        self.locationManager = [[CLLocationManager alloc] init];
    }
	
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
	self.locationManager.delegate = self;
	self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.distanceFilter = kMinimumDistanceLocationUpdate;
	
	[self.locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
	CLLocation *currentLocation = newLocation;
	
	if (currentLocation != nil)
	{
		_currentLocation = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
		if(_showPlanes)
        {
            [self planesButtonPressed];
        }

        else if(_showSeats)
        {
            [self seatsButtonPressed];
        }
 
        else if(_showFoams)
        {
            [self foamsButtonPressed];
        }
        
        if(_showList)
        {
            [self listButtonPressed];
        }
        
        else if(_showMap)
        {
            [self mapButtonPressed];
        }
	}
}

- (int) distanceBetweenLocations:(CLLocationCoordinate2D)firstLocation secondLocation:(CLLocationCoordinate2D)secondLocation
{
	CLLocation *first = [[CLLocation alloc] initWithLatitude:firstLocation.latitude longitude:firstLocation.longitude];
	CLLocation *second = [[CLLocation alloc] initWithLatitude:secondLocation.latitude longitude:secondLocation.longitude];
	
	CLLocationDistance distanceMeters = [first distanceFromLocation:second];
	double distanceMiles = (distanceMeters / kMetersPerMile);
	
	return (int) distanceMiles;
}

- (void) sortPlanesByDistance
{
	NSMutableArray *unsortedLocations = [NSMutableArray arrayWithArray:_planes];
	NSMutableArray *sortedLocations = [[NSMutableArray alloc] init];
	
	while (unsortedLocations.count > 0)
	{
		long shortestDistance = NSIntegerMax;
		NFSLocation *closestApplicator = nil;
		
		for (NFSLocation *applicator in unsortedLocations)
		{
			int distance = [self distanceBetweenLocations:_currentLocation secondLocation:applicator.coordinate];
			
			if (distance < shortestDistance)
			{
				shortestDistance = distance;
				closestApplicator = applicator;
			}
		}
		
		[sortedLocations addObject:closestApplicator];
		[unsortedLocations removeObject:closestApplicator];
	}
	
	_planes = [NSArray arrayWithArray:sortedLocations];
}

- (void) sortFoamByDistance
{
    NSMutableArray *unsortedFoam = [NSMutableArray arrayWithArray:_foams];
    NSMutableArray *sortedFoam = [[NSMutableArray alloc] init];
    
    while (unsortedFoam.count > 0)
    {
        long shortestDistance = NSIntegerMax;
        NFSFoam *closestFoam = nil;
        
        for (NFSFoam *foam in unsortedFoam)
        {
            int distance = [self distanceBetweenLocations:_currentLocation secondLocation:foam.coordinate];
            
            if (distance < shortestDistance)
            {
                shortestDistance = distance;
                closestFoam = foam;
            }
        }
        
        [sortedFoam addObject:closestFoam];
        [unsortedFoam removeObject:closestFoam];
    }
    
    _foams = [NSArray arrayWithArray:sortedFoam];
}

- (void) sortSeatsByDistance
{
    NSMutableArray *unsortedSeats = [NSMutableArray arrayWithArray:_seats];
    NSMutableArray *sortedSeats = [[NSMutableArray alloc] init];
    
    while (unsortedSeats.count > 0)
    {
        long shortestDistance = NSIntegerMax;
        NFSSeat *closestSeat = nil;
        
        for (NFSSeat *seat in unsortedSeats)
        {
            int distance = [self distanceBetweenLocations:_currentLocation secondLocation:seat.coordinate];
            
            if (distance < shortestDistance)
            {
                shortestDistance = distance;
                closestSeat = seat;
            }
        }
        
        [sortedSeats addObject:closestSeat];
        [unsortedSeats removeObject:closestSeat];
    }
    
    _seats = [NSArray arrayWithArray:sortedSeats];

}

/*
 
 Source: http://stackoverflow.com/a/12696424/529554
 */
- (float) getHeadingForDirectionFromCoordinate:(CLLocationCoordinate2D)fromLoc toCoordinate:(CLLocationCoordinate2D)toLoc
{
	float fLat = kDegreesToRadians(fromLoc.latitude);
	float fLng = kDegreesToRadians(fromLoc.longitude);
	float tLat = kDegreesToRadians(toLoc.latitude);
	float tLng = kDegreesToRadians(toLoc.longitude);
	
	float degree = kRadiansToDegrees(atan2(sin(tLng-fLng)*cos(tLat), cos(fLat)*sin(tLat)-sin(fLat)*cos(tLat)*cos(tLng-fLng)));
	
	if (degree >= 0)
	{
		return degree;
	}
	else
	{
		return 360 + degree;
	}
}

- (NSString *) bearingToCompassDirection:(float)bearing
{
	if (bearing > kNorthNorthwest || bearing < kNorthNortheast)
	{
		return kNorth;
	}
	
	if (bearing > kNorthNortheast && bearing < kEastNortheast)
	{
		return kNortheast;
	}
	
	if (bearing > kEastNortheast && bearing < kEastSoutheast)
	{
		return kEast;
	}
	
	if (bearing > kEastSoutheast && bearing < kSouthSoutheast)
	{
		return kSoutheast;
	}
	
	if (bearing > kSouthSoutheast && bearing < kSouthSouthwest)
	{
		return kSouth;
	}
	
	if (bearing > kSouthSouthwest && bearing < kWestSouthwest)
	{
		return kSouthwest;
	}
	
	if (bearing > kWestSouthwest && bearing < kWestNorthwest)
	{
		return kWest;
	}
	
	if (bearing > kWestNorthwest && bearing < kNorthNorthwest)
	{
		return kNorthwest;
	}
	
	return @"Unknown";
}

- (void) plotPlanes
{
	[self clearMap];
	
	for (NFSLocation *plane in _planes)
	{
        if([[plane.status uppercaseString] isEqual:@"ACTIVE"])
        {
            [_mapView addAnnotation:plane];
        }
	}
}

- (void) plotFoam
{
	[self clearMap];
	
	for (NFSFoam *foam in _foams)
    {
        [_mapView addAnnotation:foam];
    }
}

- (void) plotSeats
{
	[self clearMap];
    
    for (NFSSeat *seat in _seats)
    {
        [_mapView addAnnotation:seat];
    }
}

- (void) clearMap
{
	[_mapView removeAnnotations:_mapView.annotations];
}

- (void) showPlane:(NFSLocation *)plane
{
	NSString *message = [self prepareMessageForAerialApplicator:plane];
	
	UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:plane.businessName
													 message:message
													delegate:self
										   cancelButtonTitle:@"Cancel"
										   otherButtonTitles:@"Call Day Phone", nil];
	
	if (plane.alternatePhone.length > 0)
	{
		[alert addButtonWithTitle:@"Call Alternate Phone"];
	}
	
	[alert show];
}

- (void) showFoam:(NFSFoam *)foam
{
    NSString *message = [self prepareMessageForFoamRetardant:foam];
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:foam.location
                                                     message:message
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:@"Call Day Phone", nil];
    if (foam.nightPhone.length>0)
    {
        [alert addButtonWithTitle:@"Call Night Phone"];
    }
    
    [alert show];
}

- (void) showSeat:(NFSSeat *)seat
{
    NSString *message = [self prepareMessageForSeat:seat];
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:seat.county
                                                     message:message
                                                    delegate:self
                                           cancelButtonTitle:@"Cancel"
                                           otherButtonTitles:nil, nil];

    [alert show];
}

@end
