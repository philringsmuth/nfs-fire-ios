//
//  FireMapViewController.m
//  NFSYellowBook
//
//  Created by Matt Keller on 3/6/15.
//  Copyright (c) 2015 Nebraska Forest Services. All rights reserved.
//

#import "FireMapViewController.h"
#import "NFSAreaFileManager.h"
#import "NFSColorUtility.h"
#import "NFSConstants.h"

static const NSUInteger kDistanceFilter = 25; //minimum distance (meters) to receive location updates
//static const NSUInteger kMaxLocationsToKeep = 1000; // Max number of locations to keep to calculate area
static const NSUInteger kMinLocationsNeededToCalculateArea = 3; // the number of locations needed before we calculate area
//static const double kAcresPerSquareDegree = 3055493; //Calculated
static const double kAcresPerSquareDegree = 2356421; //Measured, aka it's MAGIC!!!

@interface FireMapViewController ()

@end

@implementation FireMapViewController
{
    CLLocationCoordinate2D _currentLocation;
    
    UIColor *_viewColor;
    
    BOOL _plotting;
    BOOL _paused;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.mapView.delegate = self;
    [_pauseButton setUserInteractionEnabled:NO];
    [_pauseButtonLabel setTextColor:[UIColor lightGrayColor]];
    
    _plotting = NO;
    _paused = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    self.locationManager.delegate = nil;
}

#pragma mark IBAction Methods

- (IBAction) fingerDownInView:(id)sender
{
    _viewColor = [sender backgroundColor];
    
    [sender setBackgroundColor:[NFSColorUtility colorFromHexString:kColorYellow]];
}

- (IBAction) fingerExitedView:(id)sender
{
	[sender setBackgroundColor:_viewColor];
}

- (IBAction) fingerUpInView:(id)sender
{
    [self fingerExitedView:sender];
    
    if (sender == _backButton)
    {
        [self backButtonPressed];
    }
    
    if (sender == _startButton)
    {
        [self startButtonPressed];
    }
    
    if (sender == _pauseButton)
    {
        [self pauseButtonPressed];
    }
    
    if (sender == _viewRecentsButton)
    {
        [self viewRecentsButtonPressed];
    }
}

#pragma mark MKMapViewDelegate

- (void)locationManager:(CLLocationManager*)manager didUpdateLocations:(NSArray *)locations
{
    if (locations != nil && locations.count > 0)
    {
        if (_storedLocations.count == 0)
        {
            // on the first location update only, zoom map to user location
            CLLocation *firstLocation = locations[0];
            CLLocationCoordinate2D newCoordinate = firstLocation.coordinate;
            
            MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(newCoordinate, 2000, 2000);

            [self.mapView setRegion:region animated:YES];
            
            //Save location
            for (CLLocation *newLocation in locations)
            {
                if (newLocation.horizontalAccuracy < kDistanceFilter)
                {
                    [self.storedLocations addObject:newLocation];
                }
            }
        }
        else
        {
            // This is a subsequent location update.
            for (CLLocation *newLocation in locations)
            {
                if (newLocation.horizontalAccuracy < kDistanceFilter)
                {
                    if (self.storedLocations.count > 0)
                    {
                        CLLocationCoordinate2D coords[2];
                        coords[0] = ((CLLocation *)self.storedLocations.lastObject).coordinate;
                        coords[1] = newLocation.coordinate;
                        
                        [self.mapView addOverlay:[MKPolyline polylineWithCoordinates:coords count:2] level:MKOverlayLevelAboveRoads];
                    }
                    [self.storedLocations addObject:newLocation];
                }
            }
        }
    }
}

//From breadcrumbs. Centers map on user
- (MKCoordinateRegion)coordinateRegionWithCenter:(CLLocationCoordinate2D)centerCoordinate approximateRadiusInMeters:(CLLocationDistance)radiusInMeters
{
    // Multiplying by MKMapPointsPerMeterAtLatitude at the center is only approximate, since latitude isn't fixed
    //
    double radiusInMapPoints = radiusInMeters*MKMapPointsPerMeterAtLatitude(centerCoordinate.latitude);
    MKMapSize radiusSquared = {radiusInMapPoints,radiusInMapPoints};
    
    MKMapPoint regionOrigin = MKMapPointForCoordinate(centerCoordinate);
    MKMapRect regionRect = (MKMapRect){regionOrigin, radiusSquared}; //origin is the top-left corner
    
    regionRect = MKMapRectOffset(regionRect, -radiusInMapPoints/2, -radiusInMapPoints/2);
    
    // clamp the rect to be within the world
    regionRect = MKMapRectIntersection(regionRect, MKMapRectWorld);
    
    MKCoordinateRegion region = MKCoordinateRegionForMapRect(regionRect);
    return region;
}

//Checks to see if the overlay is a polyline: http://www.raywenderlich.com/73984/make-app-like-runkeeper-part-1
- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    if ([overlay isKindOfClass:[MKPolyline class]]) {
        MKPolyline *polyLine = (MKPolyline *)overlay;
        MKPolylineRenderer *aRenderer = [[MKPolylineRenderer alloc] initWithPolyline:polyLine];
        aRenderer.strokeColor = [UIColor blueColor];
        aRenderer.lineWidth = 3;
        return aRenderer;
    }
    return nil;
}

#pragma mark UIAlertView Delegate

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        return;
    }
}

#pragma mark Private Methods

- (void) backButtonPressed
{
    [self dismissViewControllerAnimated:YES completion:nil];
    [UIApplication sharedApplication].idleTimerDisabled = NO;
}

- (void)startButtonPressed
{
    if (!_plotting)
    {
        //Remove overlays from previous runs
        [self.mapView removeOverlays:self.mapView.overlays];
        
        [_startButtonLabel setText:@"Stop and Save"];
        [_pauseButton setUserInteractionEnabled:YES];
        [_pauseButtonLabel setTextColor:[UIColor blackColor]];
        self.storedLocations = [NSMutableArray array];
        [self initializeLocationUpdates];
        _plotting = YES;
        [UIApplication sharedApplication].idleTimerDisabled = YES;
    }
    else
    {
        // stop and save
        // record is created and saved to text file
        [self.locationManager stopUpdatingLocation];
        [UIApplication sharedApplication].idleTimerDisabled = NO;
        
        //Finish the loop
        if (self.storedLocations.count > 0)
        {
            CLLocationCoordinate2D coords[2];
            coords[0] = ((CLLocation *)self.storedLocations.lastObject).coordinate;
            coords[1] = ((CLLocation *)self.storedLocations.firstObject).coordinate;
            
            [self.mapView addOverlay:[MKPolyline polylineWithCoordinates:coords count:2] level:MKOverlayLevelAboveRoads];
        }

        //Calculate acreage in enclosed loop
        if(kMinLocationsNeededToCalculateArea <= [self.storedLocations count])
        {
            double area = [self calculateArea];
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm"];
            NSString *now = [dateFormatter stringFromDate:[NSDate date]];
            
            NSString *newEntry = [NSString stringWithFormat:@"%f, %@", area, now];
            NFSAreaFileManager *areaFileManager = [[NFSAreaFileManager alloc] init];
            [areaFileManager writeNewAreaToFile:newEntry];
        }
        else
        {
            UIAlertView *alert =[[UIAlertView alloc ] initWithTitle:@"Area Too Small"
                                                            message:@"There were less than 3 points found to calculate the area with\nThe smallest area that we can calculate is about 5 acres"
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                                  otherButtonTitles:nil, nil];
            
            [alert show];
        }
        
        //Dumps all locations from last run
        [_storedLocations removeAllObjects];
        
        //Update button on UI
        [_startButtonLabel setText:@"Start"];
        
        [_pauseButton setUserInteractionEnabled:NO];
        [_pauseButtonLabel setTextColor:[UIColor lightGrayColor]];
        
        _paused = NO;
        _plotting = NO;
    }
}

- (void) pauseButtonPressed
{
    _paused = !_paused;
    
    if (_paused)
    {
        [self.locationManager stopUpdatingLocation];
        [_pauseButtonLabel setText:@"Resume"];
        [UIApplication sharedApplication].idleTimerDisabled = NO;
    }
    else
    {
        [self.locationManager startUpdatingLocation];
        [_pauseButtonLabel setText:@"Pause"];
        [UIApplication sharedApplication].idleTimerDisabled = YES;
    }
}

- (void) viewRecentsButtonPressed
{
    // takes you to a list view showing dates/times, locations and acreages of recent mappings.
    // will be saved to a local text file and only some set number of records will be kept.
    NSString *message = [self prepareRecentAreas];
    
    UIAlertView *alert =[[UIAlertView alloc ] initWithTitle:@"Recent plots\n\nAcres, Time"
                                                     message:message
                                                    delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:nil, nil];
    
    [alert show];
}

- (void)initializeLocationUpdates
{
    if(_locationManager == nil)
    {
        _locationManager = [[CLLocationManager alloc] init];
        assert(self.locationManager);
        self.locationManager.delegate = self; // tells the location manager to send updates to this object
    }
    
    self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    self.locationManager.activityType = CLActivityTypeAutomotiveNavigation;
    self.locationManager.distanceFilter = kDistanceFilter;
    // start tracking the user's location
    [self.locationManager startUpdatingLocation];
    
    // Observe the application going in and out of the background, so we can toggle location tracking.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUIApplicationDidEnterBackgroundNotification:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleUIApplicationWillEnterForegroundNotification:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

- (void)handleUIApplicationDidEnterBackgroundNotification:(NSNotification *)note
{
    [self switchToBackgroundMode:YES];
}

- (void)handleUIApplicationWillEnterForegroundNotification :(NSNotification *)note
{
    [self switchToBackgroundMode:NO];
}

// called when the app is moved to the background (user presses the home button) or to the foreground
- (void)switchToBackgroundMode:(BOOL)background
{
    //Currently just stops tracking while in the background
    if (background)
    {
        [self.locationManager stopUpdatingLocation];
    }
    else
    {
        [self.locationManager startUpdatingLocation];
    }
}

- (double)calculateArea
{
    double area = 0;
    if([self.storedLocations count] > kMinLocationsNeededToCalculateArea)
    {
    //for each location
        CLLocation *tempLocation;
        CLLocation *tempLocation2;
        double sum = 0;
        double sum2 = 0;

        //Shoelace Formula - Longitude is x, latitude is y
        //Make longitude positive for simpler checking of calculations
        //In the end that doesn't particularly matter
        for(NSUInteger i = 0; i < self.storedLocations.count - 1; i++)
        {
            tempLocation = [self.storedLocations objectAtIndex:i];
            tempLocation2 = [self.storedLocations objectAtIndex:i+1];

            sum = sum + fabs(tempLocation.coordinate.longitude)*tempLocation2.coordinate.latitude;
            sum2 = sum2 + tempLocation.coordinate.latitude * fabs(tempLocation2.coordinate.longitude);
        }
        tempLocation = [self.storedLocations objectAtIndex:self.storedLocations.count-1];
        tempLocation2 = [self.storedLocations objectAtIndex:0];

        sum = sum + fabs(tempLocation.coordinate.longitude)*tempLocation2.coordinate.latitude;
        sum2 = sum2 + tempLocation.coordinate.latitude * fabs(tempLocation2.coordinate.longitude);

        area = (sum - sum2) / 2;
        area = area * kAcresPerSquareDegree;
    }
    return ABS(area);
}

- (NSString *) prepareRecentAreas
{
    NFSAreaFileManager *areaFileManager = [[NFSAreaFileManager alloc] init];
    NSArray *areasFromFile = [areaFileManager readAreaFile];
    
    NSMutableString *areaStringTemp = [[NSMutableString alloc] init];
    
    for(NSInteger i = areasFromFile.count-1; i>-1; i--)
    {
        [areaStringTemp appendString:[areasFromFile objectAtIndex:i]];
        [areaStringTemp appendString:@"\n"];
    }
    
    NSString *areaString = areaStringTemp;
    return areaString;
}
@end
