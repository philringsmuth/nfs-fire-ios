//
//  NFSAreaFileManager.h
//  NFSYellowBook
//
//  Created by Student on 3/27/15.
//  Copyright (c) 2015 Nebraska Forest Services. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NFSAreaFileManager : NSObject

- (void) writeNewAreaToFile:(NSString *)recentArea;
- (NSArray *) readAreaFile;

@end
