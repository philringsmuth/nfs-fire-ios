//
//  NFSColorUtility.m
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 12/3/14.
//  Copyright (c) 2014 Nebraska Forest Services. All rights reserved.
//

#import "NFSColorUtility.h"

@implementation NFSColorUtility

// http://stackoverflow.com/a/12397366/529554
+ (UIColor *) colorFromHexString:(NSString *)hexString
{
	unsigned rgbValue = 0;
	NSScanner *scanner = [NSScanner scannerWithString:hexString];
	[scanner setScanLocation:0];
	[scanner scanHexInt:&rgbValue];
	return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end
