//
//  ViewController.m
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 10/8/14.
//  Copyright (c) 2014 Nebraska Forest Services. All rights reserved.
//

#import "MainViewController.h"
#import "DetailViewController.h"
#import "BillingViewController.h"
#import "FireMapViewController.h"
#import "NFSConstants.h"
#import "NFSColorUtility.h"
#import "NFSDataSource.h"

@implementation MainViewController
{
	NSArray *_planes;
	NSArray *_seats;
	NSArray *_foams;
}


#pragma mark Apple Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // Do any additional setup after loading the view, typically from a nib.
	
	[self copyFilesFromBundle];
	
	[self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
	
    // Dispose of any resources that can be recreated.
}


#pragma mark IBAction Methods

- (IBAction) fingerDownInView:(id)sender
{
	[sender setBackgroundColor:[UIColor whiteColor]];
}

- (IBAction) fingerExitedView:(id)sender
{
	[sender setBackgroundColor:[NFSColorUtility colorFromHexString:kColorYellow]];
}

- (IBAction) fingerUpInView:(id)sender
{
	[self fingerExitedView:sender];
	
	if (sender == _findPlaneButton)
	{
		[self findPlaneButtonPressed];
	}
	
	if (sender == _billingButton)
	{
		[self billingButtonPressed];
	}
    
    if (sender == _plotAcreageButton)
    {
        [self plotAcreageButtonPressed];
    }
}


#pragma mark Private Methods

- (void) copyFilesFromBundle
{
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths firstObject];
	
	NSString *planesJsonPath = [documentsDirectory stringByAppendingPathComponent:kPlanesLocalFilename];
	NSString *seatsJsonPath = [documentsDirectory stringByAppendingPathComponent:kSeatsLocalFilename];
	NSString *foamsJsonPath = [documentsDirectory stringByAppendingPathComponent:kFoamsLocalFilename];
	
	if ([fileManager fileExistsAtPath:planesJsonPath] == NO)
	{
		NSLog(@"Copying Planes");
		NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"planes_local" ofType:@"json"];
		[fileManager copyItemAtPath:resourcePath toPath:planesJsonPath error:&error];
	}
	
	if ([fileManager fileExistsAtPath:seatsJsonPath] == NO)
	{
		NSLog(@"Copying Seats");
		NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"seats_local" ofType:@"json"];
		[fileManager copyItemAtPath:resourcePath toPath:seatsJsonPath error:&error];
	}
	
	if ([fileManager fileExistsAtPath:foamsJsonPath] == NO)
	{
		NSLog(@"Copying Foams");
		NSString *resourcePath = [[NSBundle mainBundle] pathForResource:@"foams_local" ofType:@"json"];
		[fileManager copyItemAtPath:resourcePath toPath:foamsJsonPath error:&error];
	}
}

- (void) findPlaneButtonPressed
{
	DetailViewController *detailController = [[DetailViewController alloc] initWithNibName:kXibNameDetailViewController bundle:[NSBundle mainBundle]];
	
	[detailController setPlanes:_planes];
	[detailController setSeats:_seats];
	[detailController setFoams:_foams];
	
	[self presentViewController:detailController animated:YES completion:nil];
}

- (void) billingButtonPressed
{
	BillingViewController *billingController = [[BillingViewController alloc] initWithNibName:kXibNameBillingViewController bundle:[NSBundle mainBundle]];
	
	[self presentViewController:billingController animated:YES completion:nil];
}

- (void) plotAcreageButtonPressed
{
    FireMapViewController *fireMapController = [[FireMapViewController alloc] initWithNibName:kXibNameFireMapViewController bundle:[NSBundle mainBundle]];
    
    [self presentViewController:fireMapController animated:YES completion:nil];
}

- (void) loadData
{
	NFSDataSource *dataSource = [[NFSDataSource alloc] init];
	
	_planes		= [dataSource planes];
	_seats		= [dataSource seats];
	_foams		= [dataSource foams];
}

- (void) pdfTest
{
	NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"billingform" ofType:@"pdf"]];
	
	CGPDFDocumentRef document = CGPDFDocumentCreateWithURL((CFURLRef) url);
	size_t count = CGPDFDocumentGetNumberOfPages(document);
	
	if (count == 0)
	{
		NSLog(@"PDF needs at least one page");
		return;
	}
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *path = [paths firstObject];
	
	NSString *file = [path stringByAppendingPathComponent:@"output.pdf"];
	
	CGRect paperSize = CGRectMake(0, 0, 612, 792);
	
	UIGraphicsBeginPDFContextToFile(file, paperSize, nil);
	
	UIGraphicsBeginPDFPageWithInfo(paperSize, nil);
	
	CGContextRef currentContext = UIGraphicsGetCurrentContext();
	
	// flip context so page is right way up
	CGContextTranslateCTM(currentContext, 0, paperSize.size.height);
	CGContextScaleCTM(currentContext, 1.0, -1.0);
	
	CGPDFPageRef page = CGPDFDocumentGetPage(document, 1);
	
	CGContextDrawPDFPage(currentContext, page);
	
	// flip context so annotations are right way up
	CGContextScaleCTM(currentContext, 1.0, -1.0);
	CGContextTranslateCTM(currentContext, 0, -paperSize.size.height);
	
	[self addText:@"REQUESTING DEPARTMENT"					inRectangle:kRectRequestingFireDepartment];
	[self addText:@"FD OFFICER REQUESTING APPLICATOR"		inRectangle:kRectFDOfficerRequesting];
	[self addText:@"DATE/TIME REQUESTED"					inRectangle:kRectDateAndTimeRequested];
	[self addText:@"DATE/TIME DISPATCHED"					inRectangle:kRectDateAndTimeDispatched];
	[self addText:@"FIRE LOCATION DESCRIPTION"				inRectangle:kRectFireLocation];
	[self addText:@"ACRES BURNED"							inRectangle:kRectApproximateAcresBurned];
	[self addText:@"TOTAL GALLONS USED" 					inRectangle:kRectTotalGallonsRetardantUsed];
	
	[self addText:@"PLANE-001"								inRectangle:kRectAircraftRegistration1];
	[self addText:@"PLANE-002"								inRectangle:kRectAircraftRegistration2];
	[self addText:@"PLANE-003"								inRectangle:kRectAircraftRegistration3];
	[self addText:@"PLANE-004"								inRectangle:kRectAircraftRegistration4];
	[self addText:@"PLANE-005"								inRectangle:kRectAircraftRegistration5];
	[self addText:@"PLANE-006"								inRectangle:kRectAircraftRegistration6];
	[self addText:@"PLANE-007"								inRectangle:kRectAircraftRegistration7];
	
	[self addText:@"5,000"									inRectangle:kRectAircraftLoad1];
	[self addText:@"5,000"									inRectangle:kRectAircraftLoad2];
	[self addText:@"5,000"									inRectangle:kRectAircraftLoad3];
	[self addText:@"5,000"									inRectangle:kRectAircraftLoad4];
	[self addText:@"5,000"									inRectangle:kRectAircraftLoad5];
	[self addText:@"5,000"									inRectangle:kRectAircraftLoad6];
	[self addText:@"5,000"									inRectangle:kRectAircraftLoad7];
	
	[self addText:@"7"										inRectangle:kRectTotalLoads1];
	[self addText:@"7"										inRectangle:kRectTotalLoads2];
	[self addText:@"7"										inRectangle:kRectTotalLoads3];
	[self addText:@"7"										inRectangle:kRectTotalLoads4];
	[self addText:@"7"										inRectangle:kRectTotalLoads5];
	[self addText:@"7"										inRectangle:kRectTotalLoads6];
	[self addText:@"7"										inRectangle:kRectTotalLoads7];
	
	[self addText:@"6.5"									inRectangle:kRectTotalHoursFlyingTime1];
	[self addText:@"6.5"									inRectangle:kRectTotalHoursFlyingTime2];
	[self addText:@"6.5"									inRectangle:kRectTotalHoursFlyingTime3];
	[self addText:@"6.5"									inRectangle:kRectTotalHoursFlyingTime4];
	[self addText:@"6.5"									inRectangle:kRectTotalHoursFlyingTime5];
	[self addText:@"6.5"									inRectangle:kRectTotalHoursFlyingTime6];
	[self addText:@"6.5"									inRectangle:kRectTotalHoursFlyingTime7];
	
	[self addText:@"500"									inRectangle:kRectRatePerHour1];
	[self addText:@"500"									inRectangle:kRectRatePerHour2];
	[self addText:@"500"									inRectangle:kRectRatePerHour3];
	[self addText:@"500"									inRectangle:kRectRatePerHour4];
	[self addText:@"500"									inRectangle:kRectRatePerHour5];
	[self addText:@"500"									inRectangle:kRectRatePerHour6];
	[self addText:@"500"									inRectangle:kRectRatePerHour7];
	
	[self addText:@"BAD AT MATH"							inRectangle:kRectTotalAmount1];
	[self addText:@"BAD AT MATH"							inRectangle:kRectTotalAmount2];
	[self addText:@"BAD AT MATH"							inRectangle:kRectTotalAmount3];
	[self addText:@"BAD AT MATH"							inRectangle:kRectTotalAmount4];
	[self addText:@"BAD AT MATH"							inRectangle:kRectTotalAmount5];
	[self addText:@"BAD AT MATH"							inRectangle:kRectTotalAmount6];
	[self addText:@"BAD AT MATH"							inRectangle:kRectTotalAmount7];
	
	[self addText:@"ONE MILLION"							inRectangle:kRectTotalHoursBilled];
	[self addText:@"1,234"									inRectangle:kRectTotalAmountBilled];
	[self addText:@"DUSTY CROPHOPPER, INC"					inRectangle:kRectApplicatorName];
	[self addText:@"34-DLI9-452"							inRectangle:kRectFederalIDNumber];
	[self addText:@"555-06-4444"							inRectangle:kRectSocialSecurityNumber];
	
	[self addText:@"1234 BIG STREET\nNOWHERESVILLE, NEBRASKA  68588" inRectangle:kRectAddress];
	
	[self addText:@"1-800-344-9900"							inRectangle:kRectPhone];
	[self addText:@"Dustbuster Crophoptastic"				inRectangle:kRectApplicatorsSignature];
	[self addText:@"2/9/2015"								inRectangle:kRectDateSubmitted];
	[self addText:@"dusty_101@gmail.hotmail.com"			inRectangle:kRectEmail];
	
	UIGraphicsEndPDFContext();
	
	CGPDFDocumentRelease (document);
}

- (void) addText:(NSString *)text inRectangle:(CGRect)rectangle
{
    NSDictionary *dictionary = @{NSFontAttributeName : [UIFont systemFontOfSize:14.0]};
    [text drawInRect:rectangle withAttributes:dictionary];
}

@end
