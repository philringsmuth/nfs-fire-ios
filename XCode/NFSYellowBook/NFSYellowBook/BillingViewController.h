//
//  BillingViewController.h
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 2/12/15.
//  Copyright (c) 2015 Nebraska Forest Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface BillingViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, MFMailComposeViewControllerDelegate>


#pragma mark IBOutlets

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *cancelButton;
@property (strong, nonatomic) IBOutlet UIView *sendEmailButton;


#pragma mark IBAction Methods

- (IBAction) fingerDownInView:(id)sender;
- (IBAction) fingerExitedView:(id)sender;
- (IBAction) fingerUpInView:(id)sender;


#pragma mark Methods

- (void) dismissKeyboard;

@end
