//
//  NFSConstants.h
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 10/8/14.
//  Copyright (c) 2014 Nebraska Forest Services. All rights reserved.
//

#define     kHTTPMethodGet                          @ "GET"

#define		kKindPlanes								100
#define		kKindSeats								200
#define		kKindFoams								300

//#define		kPlanesTableID							@ "1nJay5XuPlL06TyJkOwlMw7eIkNEBT-tGTgIYhJch"
#define		kPlanesTableID							@ "13bV6peZGLvp7mQ_r3V8mvGrrC6cC8CZiRSW_Oryl"
#define		kFoamsTableID							@ "1zWuxpJQtQgTo3m3HRflw42N1TkBMSqxikioBtpO0"
#define		kSeatsTableID							@ "1IILFsPnQ-S6iD322ds4Y_obx19uf5rsvasZNC12V"
#define		kApiKey									@ "AIzaSyAxP0sbFJ2FX6CJOojTYADPg74h-5jvNh0"

#define		kTableAddressPrefix						@ "https://www.googleapis.com/fusiontables/v1/query?sql=SELECT%20*%20FROM%20"

#define     kFoamRetardantsFusionTableAddress       @ "UNKNOWN"
#define     kSeatsFusionTableAddress                @ "UNKNOWN"

#define		kTimeoutInterval						10

#define     kXibNameMainViewController              @ "MainViewController"
#define		kXibNameDetailViewController			@ "DetailViewController"
#define		kXibNameBillingViewController			@ "BillingViewController"
#define		kXibNameFireMapViewController           @ "FireMapViewController"

#define     kColumns                                @ "columns"
#define     kRows                                   @ "rows"

#define		kZipCodesFileName						@ "zipcodes"
#define		kCsvExtension							@ "csv"
#define		kNewLine								@ "\n"
#define		kComma									@ ","
#define		kHyphen									@ "-"

#define		kMetersPerMile							1609.34
#define		kDegreesToRadians(x)					(M_PI * x / 180.0)
#define		kRadiansToDegrees(x)					(x * 180.0 / M_PI)
#define     kMinimumDistanceLocationUpdate          1000

#define		kColorRed								@ "7D2221"
#define		kColorYellow							@ "FECE24"
#define		kColorGreen								@ "00713C"

#define		kNorth									@ "North"
#define		kNortheast								@ "Northeast"
#define		kEast									@ "East"
#define		kSoutheast								@ "Southeast"
#define		kSouth									@ "South"
#define		kSouthwest								@ "Southwest"
#define		kWest									@ "West"
#define		kNorthwest								@ "Northwest"

#define		kNorthNortheast							22.5
#define		kEastNortheast							67.5
#define		kEastSoutheast							112.5
#define		kSouthSoutheast							157.5
#define		kSouthSouthwest							202.5
#define		kWestSouthwest							247.5
#define		kWestNorthwest							292.5
#define		kNorthNorthwest							337.5

#define		kFadeDuration							0.25

#define		kLatitudeDelta							1.0
#define		kLongitudeDelta							1.0

#define		kPlanesLocalFilename					@ "planes_local.json"
#define		kSeatsLocalFilename						@ "seats_local.json"
#define		kFoamsLocalFilename						@ "foams_local.json"
#define     kAreaResultsFilename                    @ "recentAreaResults.txt"

#define		kRectRequestingFireDepartment			CGRectMake(25, 58, 265, 16)
#define		kRectFDOfficerRequesting				CGRectMake(295, 58, 290, 16)
#define		kRectDateAndTimeRequested				CGRectMake(117, 80, 174, 16)
#define		kRectDateAndTimeDispatched				CGRectMake(389, 80, 195, 16)
#define		kRectFireLocation						CGRectMake(25, 116, 555, 60)
#define		kRectApproximateAcresBurned				CGRectMake(152, 186, 137, 16)
#define		kRectTotalGallonsRetardantUsed			CGRectMake(440, 186, 148, 16)

#define		kRectAircraftRegistration1				CGRectMake(25, 245, 110, 12)
#define		kRectAircraftRegistration2				CGRectMake(25, 262, 110, 12)
#define		kRectAircraftRegistration3				CGRectMake(25, 278, 110, 12)
#define		kRectAircraftRegistration4				CGRectMake(25, 296, 110, 12)
#define		kRectAircraftRegistration5				CGRectMake(25, 314, 110, 12)
#define		kRectAircraftRegistration6				CGRectMake(25, 331, 110, 12)
#define		kRectAircraftRegistration7				CGRectMake(25, 349, 110, 12)

#define		kRectAircraftLoad1						CGRectMake(150, 245, 45, 12)
#define		kRectAircraftLoad2						CGRectMake(150, 262, 45, 12)
#define		kRectAircraftLoad3						CGRectMake(150, 278, 45, 12)
#define		kRectAircraftLoad4						CGRectMake(150, 296, 45, 12)
#define		kRectAircraftLoad5						CGRectMake(150, 314, 45, 12)
#define		kRectAircraftLoad6						CGRectMake(150, 331, 45, 12)
#define		kRectAircraftLoad7						CGRectMake(150, 349, 45, 12)

#define		kRectTotalLoads1						CGRectMake(225, 245, 60, 12)
#define		kRectTotalLoads2						CGRectMake(225, 262, 60, 12)
#define		kRectTotalLoads3						CGRectMake(225, 278, 60, 12)
#define		kRectTotalLoads4						CGRectMake(225, 296, 60, 12)
#define		kRectTotalLoads5						CGRectMake(225, 314, 60, 12)
#define		kRectTotalLoads6						CGRectMake(225, 331, 60, 12)
#define		kRectTotalLoads7						CGRectMake(225, 349, 60, 12)

#define		kRectTotalHoursFlyingTime1				CGRectMake(300, 245, 70, 12)
#define		kRectTotalHoursFlyingTime2				CGRectMake(300, 262, 70, 12)
#define		kRectTotalHoursFlyingTime3				CGRectMake(300, 278, 70, 12)
#define		kRectTotalHoursFlyingTime4				CGRectMake(300, 296, 70, 12)
#define		kRectTotalHoursFlyingTime5				CGRectMake(300, 314, 70, 12)
#define		kRectTotalHoursFlyingTime6				CGRectMake(300, 331, 70, 12)
#define		kRectTotalHoursFlyingTime7				CGRectMake(300, 349, 70, 12)

#define		kRectRatePerHour1						CGRectMake(420, 245, 50, 12)
#define		kRectRatePerHour2						CGRectMake(420, 262, 50, 12)
#define		kRectRatePerHour3						CGRectMake(420, 278, 50, 12)
#define		kRectRatePerHour4						CGRectMake(420, 296, 50, 12)
#define		kRectRatePerHour5						CGRectMake(420, 314, 50, 12)
#define		kRectRatePerHour6						CGRectMake(420, 331, 50, 12)
#define		kRectRatePerHour7						CGRectMake(420, 349, 50, 12)

#define		kRectTotalAmount1						CGRectMake(490, 245, 95, 12)
#define		kRectTotalAmount2						CGRectMake(490, 262, 95, 12)
#define		kRectTotalAmount3						CGRectMake(490, 278, 95, 12)
#define		kRectTotalAmount4						CGRectMake(490, 296, 95, 12)
#define		kRectTotalAmount5						CGRectMake(490, 314, 95, 12)
#define		kRectTotalAmount6						CGRectMake(490, 331, 95, 12)
#define		kRectTotalAmount7						CGRectMake(490, 349, 95, 12)

#define		kRectTotalHoursBilled					CGRectMake(300, 368, 100, 10)
#define		kRectTotalAmountBilled					CGRectMake(495, 368, 90, 10)
#define		kRectApplicatorName						CGRectMake(110, 385, 475, 12)
#define		kRectFederalIDNumber					CGRectMake(120, 402, 165, 14)
#define		kRectSocialSecurityNumber				CGRectMake(435, 402, 150, 14)
#define		kRectAddress							CGRectMake(25, 435, 555, 45)
#define		kRectPhone								CGRectMake(25, 495, 220, 30)
#define		kRectApplicatorsSignature				CGRectMake(260, 495, 325, 30)
#define		kRectDateSubmitted						CGRectMake(100, 535, 145, 18)
#define		kRectEmail								CGRectMake(295, 535, 290, 18)