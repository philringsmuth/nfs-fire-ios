//
//  NFSAreaFileManager.m
//  NFSYellowBook
//
//  Created by Student on 3/27/15.
//  Copyright (c) 2015 Nebraska Forest Services. All rights reserved.
//

#import "NFSAreaFileManager.h"
#import "NFSConstants.h"

@implementation NFSAreaFileManager

- (id) init
{
    self = [super init];
    
    if (self)
    {
        //Any initialization?
    }
    
    return self;
}

- (void) writeNewAreaToFile:(NSString *)recentArea
{
    NSArray *areasFromFile = [self readAreaFile];
    NSMutableString *areaStringTemp = [[NSMutableString alloc] init];
    
    NSInteger i = 0;
    //Deletes oldest entry, only keeping the 10 most recent (Change?)
    //Can't use a for loop because initialization isn't always the same
    if (areasFromFile.count > 9)
    {
        i = 1;
    }
    else
    {
        i = 0;
    }
    
    while(i<areasFromFile.count)
    {
        [areaStringTemp appendString:[areasFromFile objectAtIndex:i]];
        [areaStringTemp appendString:@"\n"];
        i++;
    }
    
    [areaStringTemp appendString:recentArea];
    
    NSString *areaString = areaStringTemp;
    [self writeAreaFile:areaString];
}

- (NSArray *) readAreaFile
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:kAreaResultsFilename];
    NSString *areas = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSArray *areasInArray = [areas componentsSeparatedByString:@"\n"];
    
    return areasInArray;
}

#pragma mark Private Methods


- (void) writeAreaFile:(NSString *)data
{
    NSError *error;
    
    NSString *filename = kAreaResultsFilename;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *file = [documentsDirectory stringByAppendingPathComponent:filename];
    [data writeToFile:file atomically:YES encoding:NSUTF8StringEncoding error:&error];
}

@end