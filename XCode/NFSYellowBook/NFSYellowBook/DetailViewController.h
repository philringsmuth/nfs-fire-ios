//
//  DetailViewController.h
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 1/21/15.
//  Copyright (c) 2015 Nebraska Forest Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <MapKit/MapKit.h>

@interface DetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, MKMapViewDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) NSArray *planes;
@property (strong, nonatomic) NSArray *seats;
@property (strong, nonatomic) NSArray *foams;
@property (strong, nonatomic) CLLocationManager *locationManager;


#pragma mark IBOutlets

@property (strong, nonatomic) IBOutlet UIView *backButton;
@property (strong, nonatomic) IBOutlet UIView *listButton;
@property (strong, nonatomic) IBOutlet UIView *mapButton;
@property (strong, nonatomic) IBOutlet UIView *planesButton;
@property (strong, nonatomic) IBOutlet UIView *seatsButton;
@property (strong, nonatomic) IBOutlet UIView *foamButton;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet MKMapView *mapView;

#pragma mark IBAction Methods

- (IBAction) fingerDownInView:(id)sender;
- (IBAction) fingerExitedView:(id)sender;
- (IBAction) fingerUpInView:(id)sender;

@end
