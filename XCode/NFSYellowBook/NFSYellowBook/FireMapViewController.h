//
//  FireMapViewController.h
//  NFSYellowBook
//
//  Created by Matt Keller on 3/6/15.
//  Copyright (c) 2015 Nebraska Forest Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface FireMapViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSMutableArray *storedLocations;

#pragma mark IBOutlets

@property (strong, nonatomic) IBOutlet UIView *backButton;
@property (strong, nonatomic) IBOutlet UIView *startButton;
@property (strong, nonatomic) IBOutlet UIView *pauseButton;
@property (strong, nonatomic) IBOutlet UIView *viewRecentsButton;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UILabel *startButtonLabel;
@property (strong, nonatomic) IBOutlet UILabel *pauseButtonLabel;


#pragma mark IBAction Methods

- (IBAction) fingerDownInView:(id)sender;
- (IBAction) fingerExitedView:(id)sender;
- (IBAction) fingerUpInView:(id)sender;

@end
