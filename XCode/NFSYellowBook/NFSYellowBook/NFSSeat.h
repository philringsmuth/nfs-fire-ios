//
//  Created by Matt Keller on 2/19/15.
//  Copyright (c) 2015 Nebraska Forest Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface NFSSeat : NSObject <MKAnnotation, NSCopying>

@property (strong, nonatomic) NSString *county;
@property (strong, nonatomic) NSString *latitudeCoordinates;
@property (strong, nonatomic) NSString *longitudeCoordinates;
@property (strong, nonatomic) NSString *status;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

- (id) initWithValues:(NSArray *)values;

- (MKMapItem *) mapItem;

@end
