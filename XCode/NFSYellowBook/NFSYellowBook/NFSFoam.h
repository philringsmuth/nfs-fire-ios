//
//  Created by Matt Keller on 2/18/15.
//  Copyright (c) 2015 Nebraska Forest Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface NFSFoam : NSObject <MKAnnotation, NSCopying>

@property (strong, nonatomic) NSString *location;
@property (strong, nonatomic) NSString *contact;
@property (strong, nonatomic) NSString *dayPhone;
@property (strong, nonatomic) NSString *nightPhone;
@property (strong, nonatomic) NSString *zipCode;
@property (strong, nonatomic) NSString *latitudeCoordinates;
@property (strong, nonatomic) NSString *longitudeCoordinates;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

- (id) initWithValues:(NSArray *)values;

- (MKMapItem *) mapItem;

@end
