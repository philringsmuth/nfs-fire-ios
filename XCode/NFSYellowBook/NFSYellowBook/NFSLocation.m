//
//  NFSLocation.m
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 1/26/15.
//  Copyright (c) 2015 Nebraska Forest Services. All rights reserved.
//

#import "NFSLocation.h"
#import "NFSConstants.h"

@implementation NFSLocation
{
	NSArray *_values;
}

- (id) initWithValues:(NSArray *)values
{
	self = [super init];
	
	if (self)
	{
		[self setPropertiesWithValues:values];
		[self setLocationData];
	}
	
	return self;
}

- (MKMapItem *) mapItem
{
	NSDictionary *addressDictionary = [[NSDictionary alloc] init];
	
	MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:_coordinate addressDictionary:addressDictionary];
	
	MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
	
	return mapItem;
}

- (void) setPropertiesWithValues:(NSArray *)values
{
	_airportCounty				= [NSString stringWithFormat:@"%@", [values objectAtIndex:0]];
	_latitudeCoordinates		= [NSString stringWithFormat:@"%@", [values objectAtIndex:1]];
	_longitudeCoordinates		= [NSString stringWithFormat:@"%@", [values objectAtIndex:2]];
	_businessName				= [NSString stringWithFormat:@"%@", [values objectAtIndex:3]];
	_ownerOperator				= [NSString stringWithFormat:@"%@", [values objectAtIndex:4]];
	_dayPhone					= [NSString stringWithFormat:@"%@", [values objectAtIndex:5]];
	_alternatePhone				= [NSString stringWithFormat:@"%@", [values objectAtIndex:6]];
	_numberOfAircraft			= [NSString stringWithFormat:@"%@", [values objectAtIndex:7]];
	_classAFoam					= [NSString stringWithFormat:@"%@", [values objectAtIndex:8]];
	_radioFrequency				= [NSString stringWithFormat:@"%@", [values objectAtIndex:9]];
	_tailNumber1				= [NSString stringWithFormat:@"%@", [values objectAtIndex:10]];
	_gallonCapacity1			= [NSString stringWithFormat:@"%@", [values objectAtIndex:11]];
	_tailNumber2				= [NSString stringWithFormat:@"%@", [values objectAtIndex:12]];
	_gallonCapacity2			= [NSString stringWithFormat:@"%@", [values objectAtIndex:13]];
	_tailNumber3				= [NSString stringWithFormat:@"%@", [values objectAtIndex:14]];
	_gallonCapacity3			= [NSString stringWithFormat:@"%@", [values objectAtIndex:15]];
	_tailNumber4				= [NSString stringWithFormat:@"%@", [values objectAtIndex:16]];
	_gallonCapacity4			= [NSString stringWithFormat:@"%@", [values objectAtIndex:17]];
	_tailNumber5				= [NSString stringWithFormat:@"%@", [values objectAtIndex:18]];
	_gallonCapacity5			= [NSString stringWithFormat:@"%@", [values objectAtIndex:19]];
	_email						= [NSString stringWithFormat:@"%@", [values objectAtIndex:20]];
	_zipCode					= [NSString stringWithFormat:@"%@", [values objectAtIndex:21]];
    _status                     = [NSString stringWithFormat:@"%@", [values objectAtIndex:22]];
	
	/*
	 Remove the hyphenated portion of the zip code, just in case it exists.
	 */
	_zipCode = [[_zipCode componentsSeparatedByString:kHyphen] firstObject];
}


/**
 Sets this Aerial Applicator's location attribute based on the data
 provided in the fusion table. If the fusion table contains values
 in the Lat Coordinates and Lon Coordinates columns, we use that to
 set the location. If it doesn't, we look it up based on the zip code
 from the fusion table.
 */
- (void) setLocationData
{
	// Assign the location attribute based on the coordinates from the fustion table
	if (_latitudeCoordinates.length > 0 && _longitudeCoordinates.length > 0)
	{
		// Separate the coordinates into components by removing whitespace separators
		NSArray *latitudeComponentsWithEmpties = [_latitudeCoordinates componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		NSArray *latitudeComponents = [latitudeComponentsWithEmpties filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
		NSArray *longitudeComponentsWithEmpties = [_longitudeCoordinates componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
		NSArray *longitudeComponents = [longitudeComponentsWithEmpties filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"length > 0"]];
		
		// Remove any non numeric/decimal characters from each component
		NSMutableArray *mutableLatitudeComponents = [[NSMutableArray alloc] init];
		NSMutableArray *mutableLongitudeComponents = [[NSMutableArray alloc] init];
		
		for (NSString *component in latitudeComponents)
		{
			NSString *cleanedString = [[component componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet]] componentsJoinedByString:@""];
			[mutableLatitudeComponents addObject:cleanedString];
		}
		
		for (NSString *component in longitudeComponents)
		{
			NSString *cleanedString = [[component componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet]] componentsJoinedByString:@""];
			[mutableLongitudeComponents addObject:cleanedString];
		}
		
		while (mutableLatitudeComponents.count < 3)
		{
			[mutableLatitudeComponents addObject:@"0"];
		}
		
		while (mutableLongitudeComponents.count < 3)
		{
			[mutableLongitudeComponents addObject:@"0"];
		}
		
		float latitudeDegrees = [[mutableLatitudeComponents objectAtIndex:0] floatValue];
		float latitudeMinutes = [[mutableLatitudeComponents objectAtIndex:1] floatValue];
		float latitudeSeconds = [[mutableLatitudeComponents objectAtIndex:2] floatValue];
		
		latitudeMinutes = latitudeMinutes + (latitudeSeconds / 60);
		latitudeDegrees = latitudeDegrees + (latitudeMinutes / 60);
		
		float longitudeDegrees = [[mutableLongitudeComponents objectAtIndex:0] floatValue];
		float longitudeMinutes = [[mutableLongitudeComponents objectAtIndex:1] floatValue];
		float longitudeSeconds = [[mutableLongitudeComponents objectAtIndex:2] floatValue];
		
		longitudeMinutes = longitudeMinutes + (longitudeSeconds / 60);
		longitudeDegrees = longitudeDegrees + (longitudeMinutes / 60);
		
		if (longitudeDegrees > 0)
		{
			longitudeDegrees = -longitudeDegrees;
		}
		
		_coordinate = CLLocationCoordinate2DMake(latitudeDegrees, longitudeDegrees);
	}
	
	// Look up location data using zipcodes.csv
	else
	{
		NSString *path = [[NSBundle mainBundle] pathForResource:kZipCodesFileName ofType:kCsvExtension];
		NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
		
		NSArray *rows = [content componentsSeparatedByString:kNewLine];
		
		for (NSString *row in rows)
		{
			NSArray *rowComponents = [row componentsSeparatedByString:kComma];
			
			if ([[rowComponents firstObject] isEqualToString:_zipCode])
			{
				NSString *latitudeString	= [rowComponents objectAtIndex:1];
				NSString *longitudeString	= [rowComponents objectAtIndex:2];
				
				float latitude = [latitudeString floatValue];
				float longitude = [longitudeString floatValue];
				
				_coordinate = CLLocationCoordinate2DMake(latitude, longitude);
			}
		}
	}
}

/**
 Required because we implement NSCopying protocol.
 Reference: http://stackoverflow.com/a/4472949/529554
 */
- (id) copyWithZone:(NSZone *)zone
{
	return [[[self class] allocWithZone:zone] initWithValues:[NSArray arrayWithObjects:_values, nil]];
}

@end
