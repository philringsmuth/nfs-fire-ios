//
//  ViewController.h
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 10/8/14.
//  Copyright (c) 2014 Nebraska Forest Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MessageUI/MessageUI.h>

@interface MainViewController : UIViewController <MFMailComposeViewControllerDelegate>


#pragma mark IBOutlets

@property (strong, nonatomic) IBOutlet UIView *findPlaneButton;
@property (strong, nonatomic) IBOutlet UIView *billingButton;
@property (strong, nonatomic) IBOutlet UIView *plotAcreageButton;


#pragma mark IBAction Methods

- (IBAction) fingerDownInView:(id)sender;
- (IBAction) fingerExitedView:(id)sender;
- (IBAction) fingerUpInView:(id)sender;

@end

