//
//  NFSDataSource.h
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 10/28/14.
//  Copyright (c) 2014 Nebraska Forest Services. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NFSDataSource : NSObject


#pragma mark Properties

@property (strong, nonatomic, readonly) NSArray *planes;
@property (strong, nonatomic, readonly) NSArray *seats;
@property (strong, nonatomic, readonly) NSArray *foams;

@end
