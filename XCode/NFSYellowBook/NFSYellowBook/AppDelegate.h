//
//  AppDelegate.h
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 10/8/14.
//  Copyright (c) 2014 Nebraska Forest Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) MFMailComposeViewController *globalMailComposer;

-(void) cycleTheGlobalMailComposer;

@end

