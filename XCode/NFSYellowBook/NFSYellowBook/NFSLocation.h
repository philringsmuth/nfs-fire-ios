//
//  NFSLocation.h
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 1/26/15.
//  Copyright (c) 2015 Nebraska Forest Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface NFSLocation : NSObject <MKAnnotation, NSCopying>

@property (strong, nonatomic) NSString *airportCounty;
@property (strong, nonatomic) NSString *latitudeCoordinates;
@property (strong, nonatomic) NSString *longitudeCoordinates;
@property (strong, nonatomic) NSString *businessName;
@property (strong, nonatomic) NSString *ownerOperator;
@property (strong, nonatomic) NSString *dayPhone;
@property (strong, nonatomic) NSString *alternatePhone;
@property (strong, nonatomic) NSString *numberOfAircraft;
@property (strong, nonatomic) NSString *classAFoam;
@property (strong, nonatomic) NSString *radioFrequency;
@property (strong, nonatomic) NSString *tailNumber1;
@property (strong, nonatomic) NSString *gallonCapacity1;
@property (strong, nonatomic) NSString *tailNumber2;
@property (strong, nonatomic) NSString *gallonCapacity2;
@property (strong, nonatomic) NSString *tailNumber3;
@property (strong, nonatomic) NSString *gallonCapacity3;
@property (strong, nonatomic) NSString *tailNumber4;
@property (strong, nonatomic) NSString *gallonCapacity4;
@property (strong, nonatomic) NSString *tailNumber5;
@property (strong, nonatomic) NSString *gallonCapacity5;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *zipCode;
@property (strong, nonatomic) NSString *status;

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;

- (id) initWithValues:(NSArray *)values;

- (MKMapItem *) mapItem;

@end
