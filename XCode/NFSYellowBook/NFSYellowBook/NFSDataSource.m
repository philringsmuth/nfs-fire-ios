//
//  NFSDataSource.m
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 10/28/14.
//  Copyright (c) 2014 Nebraska Forest Services. All rights reserved.
//

#import "NFSDataSource.h"
#import "NFSConstants.h"
#import "NFSLocation.h"
#import "NFSFoam.h"
#import "NFSSeat.h"

@implementation NFSDataSource
{
	BOOL _remoteLoaded;
}

- (id) init
{
    self = [super init];
    
    if (self)
    {
		[self loadPlanesDataFromLocal];
		[self loadSeatsDataFromLocal];
		[self loadFoamsDataFromLocal];
		
		[self fetchRemoteData];
    }
    
    return self;
}


#pragma mark Private Methods

- (void) fetchRemoteData
{
	[self loadPlanesDataFromRemote];
	[self loadSeatsDataFromRemote];
	[self loadFoamsDataFromRemote];
}

- (void) loadPlanesDataFromRemote
{
	NSString *planesString = [self loadRemoteDataFromTable:kPlanesTableID];
	
	if (_remoteLoaded)
	{
		[self saveLocalCopy:planesString toFile:kPlanesLocalFilename];
		
		_planes = [self loadDataFromString:planesString forKind:kKindPlanes];
	}
}

- (void) loadSeatsDataFromRemote
{
	NSString *seatsString = [self loadRemoteDataFromTable:kSeatsTableID];
	
	if (_remoteLoaded)
	{
		[self saveLocalCopy:seatsString toFile:kSeatsLocalFilename];
		
		_seats = [self loadDataFromString:seatsString forKind:kKindSeats];
	}
}

- (void) loadFoamsDataFromRemote
{
	NSString *foamsString = [self loadRemoteDataFromTable:kFoamsTableID];

	if (_remoteLoaded)
	{
		[self saveLocalCopy:foamsString toFile:kFoamsLocalFilename];
		
		_foams = [self loadDataFromString:foamsString forKind:kKindFoams];
	}
}

- (void) loadPlanesDataFromLocal
{
	NSString *planesString = [self loadLocalDataFromFile:kPlanesLocalFilename];
	
	_planes = [self loadDataFromString:planesString forKind:kKindPlanes];
}

- (void) loadSeatsDataFromLocal
{
	NSString *seatsString = [self loadLocalDataFromFile:kSeatsLocalFilename];
	
	_seats = [self loadDataFromString:seatsString forKind:kKindSeats];
}

- (void) loadFoamsDataFromLocal
{
	NSString *foamsString = [self loadLocalDataFromFile:kFoamsLocalFilename];
	
	_foams = [self loadDataFromString:foamsString forKind:kKindFoams];
}


- (NSString *) urlStringForTableId:(NSString *)tableId
{
	return [NSString stringWithFormat:@"%@%@&key=%@", kTableAddressPrefix, tableId, kApiKey];
}

- (void) saveLocalCopy:(NSString *)jsonString toFile:(NSString *)filename
{
	NSError *error;
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *file = [documentsDirectory stringByAppendingPathComponent:filename];
	[jsonString writeToFile:file atomically:YES encoding:NSUTF8StringEncoding error:&error];
}

- (NSString *) loadLocalDataFromFile:(NSString *)filename
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *filePath = [documentsDirectory stringByAppendingPathComponent:filename];
	NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
	
	return content;
}

- (NSString *) loadRemoteDataFromTable:(NSString *)tableID
{
	_remoteLoaded = true;
	
	NSString *url = [self urlStringForTableId:tableID];
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
														   cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
													   timeoutInterval:kTimeoutInterval];
	
	[request setHTTPMethod:kHTTPMethodGet];
	
	NSError *error;
	NSURLResponse *urlResponse = nil;
	NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
	NSString *jsonString = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
	
	if (error)
	{
		_remoteLoaded = false;
	}
	
	return jsonString;
}

- (NSArray *) loadDataFromString:(NSString *)jsonString forKind:(int)kind
{
	NSError *error;
	NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
	NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
	NSArray *columnNames = [dictionary objectForKeyedSubscript:kColumns];
	NSMutableArray *results = [[NSMutableArray alloc] init];
	
	for (NSArray *row in [dictionary objectForKey:kRows])
	{
		NSMutableArray *values = [[NSMutableArray alloc] init];
		
		for (int i = 0; i < columnNames.count; i++)
		{
			NSString *columnValue = [row objectAtIndex:i];
			[values addObject:columnValue];
		}
		
		switch(kind)
		{
			case kKindPlanes:
			{
				NFSLocation *location = [[NFSLocation alloc] initWithValues:[NSArray arrayWithArray:values]];
				[results addObject:location];
				break;
			}
				
			case kKindSeats:
			{
				NFSSeat *seat = [[NFSSeat alloc] initWithValues:[NSArray arrayWithArray:values]];
				[results addObject:seat];
				break;
			}
				
			case kKindFoams:
			{
				NFSFoam *foam = [[NFSFoam alloc] initWithValues:[NSArray arrayWithArray:values]];
				[results addObject:foam];
				break;
			}
		}
    }
	
	return [NSArray arrayWithArray:results];
}

@end
