//
//  BillingViewController.m
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 2/12/15.
//  Copyright (c) 2015 Nebraska Forest Services. All rights reserved.
//

#import "BillingViewController.h"
#import "BillingTableViewCell.h"
#import "NFSConstants.h"
#import "NFSColorUtility.h"
#import <MessageUI/MessageUI.h>
#import "AppDelegate.h"

@implementation BillingViewController
{
	NSArray *_labels;
	NSMutableArray *_userInputText;
	AppDelegate *_appDelegate;
}


#pragma mark Apple Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	_appDelegate = [[UIApplication sharedApplication] delegate];
	
	[self initializeLabels];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark IBAction Methods

- (IBAction) fingerDownInView:(id)sender
{
	[sender setBackgroundColor:[NFSColorUtility colorFromHexString:kColorYellow]];
}

- (IBAction) fingerExitedView:(id)sender
{
	[sender setBackgroundColor:[UIColor whiteColor]];
}

- (IBAction) fingerUpInView:(id)sender
{
	[self fingerExitedView:sender];
	
	if (sender == _cancelButton)
	{
		[self dismissViewControllerAnimated:YES completion:nil];
	}
	
	if (sender == _sendEmailButton)
	{
		[self generateBill];
	}
}


#pragma mark Other Public Methods

- (void) dismissKeyboard
{
	[self.view endEditing:YES];
}


#pragma mark Tableview Datasource Methods

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 18 + 1;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == 18)
	{
		return 160;
	}
	
	return 80;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	BillingTableViewCell *cell = (BillingTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"BillingCell"];
	
	if (indexPath.row == 18)
	{
		UITableViewCell *blankCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
		[blankCell setUserInteractionEnabled:NO];
		
		return blankCell;
	}
	
	if (cell == nil)
	{
		NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"BillingCell" owner:self options:nil];
		id firstObject = [topLevelObjects firstObject];
		if ( [firstObject isKindOfClass:[UITableViewCell class]] )
		{
			cell = firstObject;
		}
		else
		{
			cell = [topLevelObjects objectAtIndex:1];
		}
	}
	
	[cell setBillingController:self];
	[cell.cellTitle setText:[_labels objectAtIndex:indexPath.row]];
	[cell.textField setText:[_userInputText objectAtIndex:indexPath.row]];
	[cell.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
	
	return cell;
}

#pragma mark Tableview Delegate Methods


#pragma mark MFMailComposeViewControllerDelegate

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
	[controller dismissViewControllerAnimated:YES completion:^
	 { [_appDelegate cycleTheGlobalMailComposer]; }
	 ];
}


#pragma mark Private Methods

- (void) initializeLabels
{
	_labels = [[NSArray alloc] initWithObjects:
			   @"Requesting Fire Department",
			   @"FD Officer Requesting Aerial Applicator",
			   @"Date and Time Aircraft Requested",
			   @"Date and Time Aircraft Dispatched",
			   @"Fire Location",
			   @"Approximate Acres Burned",
			   @"Total Gallons Retardant Used",
			   @"Aircraft Registration No.",
			   @"Aircraft Load",
			   @"Total Loads",
			   @"Total Hours Flying Time",
			   @"Rate Per Hour",
			   @"Total Amount",
			   @"Applicator Name",
			   @"Federal ID Number",
			   @"Social Security Number",
			   @"Address",
			   @"Phone",
			   nil];
	
	_userInputText = [[NSMutableArray alloc] init];
	
    for (NSInteger i = 0; i < _labels.count; i++)
	{
		[_userInputText addObject:@""];
	}
}

- (void) textFieldDidChange:(id)sender
{
	UITextField *textField = (UITextField *) sender;
	
	NSIndexPath *indexPath = [_tableView indexPathForCell:(BillingTableViewCell *) textField.superview.superview.superview];
	
	[_userInputText setObject:textField.text atIndexedSubscript:indexPath.row];
}

- (void) generateBill
{
	NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"billingform" ofType:@"pdf"]];
	
	CGPDFDocumentRef document = CGPDFDocumentCreateWithURL((CFURLRef) url);
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *path = [paths firstObject];
	
	NSString *file = [path stringByAppendingPathComponent:@"bill.pdf"];
	
	NSLog(@"Path: %@", path);
	
	CGRect paperSize = CGRectMake(0, 0, 612, 792);
	
	UIGraphicsBeginPDFContextToFile(file, paperSize, nil);
	
	UIGraphicsBeginPDFPageWithInfo(paperSize, nil);
	
	CGContextRef currentContext = UIGraphicsGetCurrentContext();
	
	// flip context so page is right way up
	CGContextTranslateCTM(currentContext, 0, paperSize.size.height);
	CGContextScaleCTM(currentContext, 1.0, -1.0);
	
	CGPDFPageRef page = CGPDFDocumentGetPage(document, 1);
	
	CGContextDrawPDFPage(currentContext, page);
	
	// flip context so annotations are right way up
	CGContextScaleCTM(currentContext, 1.0, -1.0);
	CGContextTranslateCTM(currentContext, 0, -paperSize.size.height);
	
	[self addTextToPdf:0  inRectangle:kRectRequestingFireDepartment];
	[self addTextToPdf:1  inRectangle:kRectFDOfficerRequesting];
	[self addTextToPdf:2  inRectangle:kRectDateAndTimeRequested];
	[self addTextToPdf:3  inRectangle:kRectDateAndTimeDispatched];
	[self addTextToPdf:4  inRectangle:kRectFireLocation];
	[self addTextToPdf:5  inRectangle:kRectApproximateAcresBurned];
	[self addTextToPdf:6  inRectangle:kRectTotalGallonsRetardantUsed];
	[self addTextToPdf:7  inRectangle:kRectAircraftRegistration1];
	[self addTextToPdf:8  inRectangle:kRectAircraftLoad1];
	[self addTextToPdf:9  inRectangle:kRectTotalLoads1];
	[self addTextToPdf:10 inRectangle:kRectTotalHoursFlyingTime1];
	[self addTextToPdf:11 inRectangle:kRectRatePerHour1];
	[self addTextToPdf:12 inRectangle:kRectTotalAmount1];
	[self addTextToPdf:13 inRectangle:kRectApplicatorName];
	[self addTextToPdf:14 inRectangle:kRectFederalIDNumber];
	[self addTextToPdf:15 inRectangle:kRectSocialSecurityNumber];
	[self addTextToPdf:16 inRectangle:kRectAddress];
	[self addTextToPdf:17 inRectangle:kRectPhone];
	
	UIGraphicsEndPDFContext();
	
	CGPDFDocumentRelease(document);
	
	NSData *data = [NSData dataWithContentsOfFile:file];
	
	if ([MFMailComposeViewController canSendMail])
	{
		_appDelegate.globalMailComposer.mailComposeDelegate = self;
		[_appDelegate.globalMailComposer setSubject:@"Fire Report"];
		[_appDelegate.globalMailComposer setMessageBody:@"" isHTML:NO];
		[_appDelegate.globalMailComposer setToRecipients:@[@"nfsfire@unl.edu"]];
		[_appDelegate.globalMailComposer addAttachmentData:data mimeType:@"application/pdf" fileName:@"bill.pdf"];
		
		[self presentViewController:_appDelegate.globalMailComposer animated:YES completion:nil];
	}
	else
	{
		NSLog(@"This device cannot send email");
		[_appDelegate cycleTheGlobalMailComposer];
	}
}

- (void) addTextToPdf:(int)index inRectangle:(CGRect)rectangle
{
	NSString *text = (NSString *)[_userInputText objectAtIndex:index];
    NSDictionary *dictionary = @{NSFontAttributeName : [UIFont systemFontOfSize:14.0]};
	[text drawInRect:rectangle withAttributes:dictionary];
}

@end
