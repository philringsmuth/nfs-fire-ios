//
//  NFSColorUtility.h
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 12/3/14.
//  Copyright (c) 2014 Nebraska Forest Services. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NFSColorUtility : NSObject

+ (UIColor *) colorFromHexString:(NSString *)hexString;

@end
