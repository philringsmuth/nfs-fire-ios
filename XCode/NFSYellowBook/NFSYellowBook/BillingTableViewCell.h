//
//  BillingTableViewCell.h
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 2/12/15.
//  Copyright (c) 2015 Nebraska Forest Services. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BillingViewController.h"

@interface BillingTableViewCell : UITableViewCell


#pragma mark IBOutlets

@property (strong, nonatomic) IBOutlet UIView *backgroundControl;
@property (strong, nonatomic) IBOutlet UILabel *cellTitle;
@property (strong, nonatomic) IBOutlet UITextField *textField;


#pragma mark Properties

@property (strong, nonatomic) BillingViewController *billingController;


#pragma mark IBAction Methods

- (IBAction) dismissKeyboard:(id)sender;

@end
