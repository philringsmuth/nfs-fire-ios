//
//  BillingTableViewCell.m
//  NFSYellowBook
//
//  Created by Phil Ringsmuth on 2/12/15.
//  Copyright (c) 2015 Nebraska Forest Services. All rights reserved.
//

#import "BillingTableViewCell.h"

@implementation BillingTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


#pragma mark IBAction Methods

- (IBAction) dismissKeyboard:(id)sender
{
	[_billingController dismissKeyboard];
}

@end
